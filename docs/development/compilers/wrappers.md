# Compiler Wrappers

NERSC provides compiler wrappers on Cori which combine the [native
compilers](native.md) (Intel, GNU, and Cray) with MPI and various other
libraries, to enable streamlined compilation of scientific applications.

## Cray Compiler Wrappers

Cray provides a convenient set of wrapper commands that should be used in
almost all cases for compiling and linking parallel programs. Invoking the
wrappers will automatically link codes with MPI libraries and other Cray system
software. All MPI and Cray system directories are also transparently imported.
In addition, the wrappers cross-compile for the appropriate compute node
architecture, based on which `craype-<arch>` module is loaded when the compiler
is invoked, where the possible values of `<arch>` are discussed below.

!!! note "Compiler wrappers target compute nodes, not login nodes"
	The intention is that programs are compiled on the login nodes and executed
	on the compute nodes. Because the compute nodes and login nodes have
	different hardware and software, executables cross-compiled for compute
	nodes may fail if run on login nodes. The wrappers mentioned above
	guarantee that codes compiled using the wrappers are prepared for running
	on the compute nodes.

!!! warning "KNL-specific compiler flags should be used for codes running on KNL nodes"
	On Cori there are two types of compute nodes: Haswell and KNL. While
	applications cross-compiled for Haswell do run on KNL compute nodes, the
	converse is not true (applications compiled for KNL will fail if run on
	Haswell compute nodes). Additionally, even though a code compiled for
	Haswell will run on a KNL node, it will not be able to take advantage of
	the wide vector processing units available on KNL. Consequently, one should
	specifically target KNL nodes during compilation in order to achieve the
	highest possible code performance. Please see below for more information on
	how to compile for KNL compute nodes.

### Basic Example

The Cray compiler wrappers replace other compiler wrappers commonly found on
computer clusters, such as `mpif90`, `mpicc`, and `mpic++`. By default, the
Cray wrappers include MPI libraries and header files, as well as the many
scientific libraries included in Cray [LibSci](../libraries/libsci/index.md).

#### Fortran

```shell
ftn -o example.x example.f90
```

#### C

```shell
cc -o example.x example.c
```

#### C++

```shell
CC -o example.x example.cpp
```

!!! note "Cray compiler wrappers now build dynamically linked executables by default"
    By default (from `cdt/19.06` and later), the Cray compiler wrappers build
    dynamically linked executables.  To build statically linked executables, just
    add the `-static` flag to the command and link lines, or set
    `CRAYPE_LINK_TYPE=static` in the environment,
    
    ```shell
    cc -static -o example.x example.c
    ```

    or

    ```shell
    export CRAYPE_LINK_TYPE=static
    cc -o example.x example.c
    ```

!!! tip "Using compiler wrappers in `./configure`"
	When compiling an application which uses the standard series of `./configure`,
	`make`, and `make install`, often specifying the compiler wrappers in the
	appropriate environment variables is sufficient for a configure step to
	succeed:
	
	```shell
	./configure CC=cc CXX=CC FC=ftn		
	```

!!! note "Use `cdt` modules to control versions of Cray-provided modulefiles"
    To use a non-default CDT version, which includes `craype`, `cray-libsci`,
    `cray-mpich`, etc. from the specific CDT version, one could issue the
    following commands first:
    
    ```shell
    module load cdt/<the-non-default-version>
    export LD_LIBRARY_PATH=$CRAY_LD_LIBRARY_PATH:$LD_LIBRARY_PATH 
    ```    
    then compile and run as usual.    

## Intel Compiler Wrappers

Although the Cray compiler wrappers `cc`, `CC`, and `ftn`, are the default (and
recommended) compiler wrappers on the Cori system, wrappers for
Intel MPI are provided as well via the the `impi` module.

The Intel MPI wrapper commands are `mpiicc`, `mpiicpc`, and `mpiifort`, which
are analogous to `cc`, `CC`, and `ftn` from the Cray wrappers, respetively. 
Same as the Cray wrappers, the default link type for the Intel wrappers is
dynamic, not static.

!!! warning "Intel MPI may be slower than Cray MPI on Cray systems"
	Although Intel MPI is available on the Cray systems at NERSC, it is not
	tuned for high performance on the high speed network on these systems.
	Consequently, it is possible, even likely, that MPI application performance
	will be lower if compiled with Intel MPI than with Cray MPI.

!!! note "Intel MPI wrappers work only with Intel compilers"
	If one chooses to use the Intel MPI compiler wrappers, they are compatible
	only with the Intel compilers `icc`, `icpc`, and `ifort`. They are
	incompatible with the Cray and GCC compilers.

!!! note "Intel MPI wrappers must specify architecture flags explicitly"
	While the Cray compiler wrappers cross-compile source code for the appropriate
	architecture based on the `craype-<arch>` modules (e.g., `craype-haswell` for
	Haswell code and `craype-mic-knl` for KNL code), the Intel wrappers do not. The
	user must apply the appropriate architecture flags to the wrappers manually,
	e.g., adding the `-xMIC-AVX512` flag to compile for KNL.

!!! warning "Intel MPI wrappers do not link to Cray libraries by default"
	Unlike the Cray compiler wrappers, the Intel compiler wrappers do not
	automatically include and link to scientific libraries such as LibSci.
	These libraries must be included and linked manually if using the Intel MPI
	wrappers.

### Compiling

The Intel compiler wrappers function similarly to the Cray wrappers `cc`, `CC`,
and `ftn`. However a few extra steps are required. To compile with the Intel
MPI wrappers, one must first load the `impi` module.

```shell
module load impi
mpiifort -xMIC-AVX512 -o example.x example.f90
```

### Running

To run an application compiled with Intel MPI, one must load the
`impi` module, and then issue the same `srun` commands as typical for
an application compiled with the Cray wrappers
([example](../../jobs/examples/index.md#intel-mpi)).
