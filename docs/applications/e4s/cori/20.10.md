# E4S 20.10 - Cori
  
## Access via Modules

In order to access e4s you can load module `e4s/20.10`, which will set up a Spack environment and 
activate the Spack environment `e4s-2010`.

```bash
$ module load e4s/20.10
$ spack env list
==> 1 environments
    e4s-2010  
$ spack env st
==> In environment e4s-2010
$ spack --version
0.15.4-1620-e1e0bbb4c
```

## E4S 20.10 Stack

!!! note
    The E4S 20.10 stack is built for the Haswell architecture. 
    
!!! note
    E4S 20.10 stack is built with the `intel/19.1.2.254` compiler and `cray-mpich/7.7.10` MPI 
    wrapper. We don't plan to build the stack with every compiler + MPI permutation.

!!! note
    e4s/20.10 stack is built with spack version `0.15.4-1620-e1e0bbb4c`. 
      
You can run `spack find` to see installed specs in E4S 20.10 release. Shown below are
the root specs provided by Spack.

```bash
$ spack find
==> In environment e4s-2010
==> Root specs
-- cray-cnl7-haswell / intel@19.1.2.254 -------------------------
adios2@2.6.0%intel@19.1.2.254 
aml@0.1.0%intel@19.1.2.254 
arborx@0.9-beta%intel@19.1.2.254 +openmp
bolt@1.0%intel@19.1.2.254 
caliper@2.4.0%intel@19.1.2.254 
darshan-runtime@3.2.1%intel@19.1.2.254 +slurm
darshan-util@3.2.1%intel@19.1.2.254 +bzip2
flit@2.1.0%intel@19.1.2.254 
gasnet@2020.3.0%intel@19.1.2.254 +udp
ginkgo@1.2.0%intel@19.1.2.254 
globalarrays@5.7%intel@19.1.2.254 +blas+lapack+scalapack
gotcha@1.0.3%intel@19.1.2.254 +test
hdf5@1.10.6%intel@19.1.2.254 +cxx+debug+fortran+hl+szip+threadsafe
hypre@2.20.0%intel@19.1.2.254 +mixedint
kokkos@3.2.00%intel@19.1.2.254 +compiler_warnings+debug+debug_dualview_modify_check+examples+hwloc+memkind+numactl+openmp+pic+tests
kokkos-kernels@3.2.00%intel@19.1.2.254 +mkl+openmp
libnrm@0.1.0%intel@19.1.2.254 
libquo@1.3.1%intel@19.1.2.254 
mercury@1.0.1%intel@19.1.2.254 +udreg
mfem@4.1.0%intel@19.1.2.254 +examples+gnutls+gslib+lapack+libunwind+openmp+pumi+threadsafe+umpire
ninja@1.10.1%intel@19.1.2.254 
openpmd-api@0.12.0%intel@19.1.2.254 
papi@6.0.0.1%intel@19.1.2.254 +example+static_tools
parallel-netcdf@1.12.1%intel@19.1.2.254 
pdt@3.25.1%intel@19.1.2.254 +pic
petsc@3.14.0%intel@19.1.2.254 
pumi@2.2.2%intel@19.1.2.254 +fortran
py-libensemble@0.7.0%intel@19.1.2.254 +mpi+nlopt+scipy
py-petsc4py@3.13.0%intel@19.1.2.254 
qthreads@1.14%intel@19.1.2.254 ~hwloc
raja@0.12.1%intel@19.1.2.254 
slepc@3.14.0%intel@19.1.2.254 
stc@0.8.3%intel@19.1.2.254 
sundials@5.4.0%intel@19.1.2.254 +examples-cxx+examples-f2003~examples-f77+f2003+hypre+klu+lapack+openmp
superlu@5.2.1%intel@19.1.2.254 
superlu-dist@6.3.1%intel@19.1.2.254 
swig@4.0.2%intel@19.1.2.254 
sz@2.1.10%intel@19.1.2.254 +fortran+hdf5+netcdf+pastri+python+random_access+time_compression
tasmanian@7.3%intel@19.1.2.254 +blas+fortran+mpi+python+xsdkflags
turbine@1.2.3%intel@19.1.2.254 +hdf5+python
umap@2.1.0%intel@19.1.2.254 +tests
umpire@4.0.1%intel@19.1.2.254 +fortran+numa+openmp
upcxx@2020.3.0%intel@19.1.2.254 
veloc@1.4%intel@19.1.2.254 
zfp@0.5.5%intel@19.1.2.254 

==> 135 installed packages
-- cray-cnl7-haswell / intel@19.1.2.254 -------------------------
adiak@0.1.1      c-blosc@1.17.0         globalarrays@5.7      kokkos@3.2.00          libszip@2.1.1        netcdf-c@4.7.4          pdt@3.25.1            py-scipy@1.5.3        suite-sparse@5.7.2  umpire@4.0.1
adios2@2.6.0     caliper@2.4.0          gmake@4.2.1           kokkos@3.2.00          libtool@2.4.6        nettle@3.4.1            perl@5.30.3           py-setuptools@50.1.0  sundials@5.4.0      umpire@4.0.1
adlbx@0.9.2      cmake@3.16.5           gmp@6.1.2             kokkos-kernels@3.2.00  libunwind@1.4.0      ninja@1.10.1            petsc@3.13.6          py-toml@0.10.0        superlu@5.2.1       upcxx@2020.3.0
aml@0.1.0        darshan-runtime@3.2.1  gnutls@3.6.14         kvtree@1.0.2           libuuid@1.0.3        nlohmann-json@3.9.1     petsc@3.14.0          python@3.8.6          superlu-dist@6.3.0  veloc@1.4
ant@1.10.7       darshan-util@3.2.1     gotcha@1.0.3          libbsd@0.10.0          libzmq@4.3.2         nlopt@2.6.1             pkg-config@0.29.2     qthreads@1.14         superlu-dist@6.3.1  xz@5.2.5
arborx@0.9-beta  diffutils@3.7          gslib@1.0.6           libfabric@1.11.0       lz4@1.9.2            numactl@2.0.12          pumi@2.2.2            raja@0.12.1           swig@4.0.2          zfp@0.5.5
argobots@1.0     er@0.0.3               hdf5@1.10.6           libffi@3.3             m4@1.4.18            openjdk@11.0.2          pumi@2.2.2            rankstr@0.0.2         sz@2.0.2.0          zlib@1.2.11
arpack-ng@3.7.0  exmcutils@0.5.7        hdf5@1.10.7           libiconv@1.16          memkind@1.7.0        openpmd-api@0.12.0      py-cython@0.29.21     readline@8.0          sz@2.1.10           zsh@5.8
autoconf@2.69    expat@2.2.9            hdf5@1.10.7           libnrm@0.1.0           mercury@1.0.1        openssl@1.1.1g          py-libensemble@0.7.0  redset@0.0.3          tasmanian@7.3       zstd@1.4.5
automake@1.16.2  flit@2.1.0             hwloc@2.2.0           libpfm4@4.11.0         metis@5.1.0          papi@6.0.0.1            py-mpi4py@3.0.3       shuffile@0.0.3        tcl@8.6.10
axl@0.3.0        gasnet@2020.3.0        hypre@2.18.2          libpng@1.6.37          mfem@4.1.0           papi@6.0.0.1            py-numpy@1.19.2       slepc@3.14.0          texinfo@6.5
bolt@1.0         gdbm@1.18.1            hypre@2.20.0          libpthread-stubs@0.4   mpark-variant@1.4.0  parallel-netcdf@1.12.1  py-petsc4py@3.13.0    snappy@1.1.8          turbine@1.2.3
boost@1.74.0     gettext@0.21           hypre@2.20.0          libquo@1.3.1           mpich@3.1            parmetis@4.0.3          py-pybind11@2.5.0     sqlite@3.31.1         turbine@1.2.3
bzip2@1.0.8      ginkgo@1.2.0           intel-mkl@19.1.2.254  libsodium@1.0.18       ncurses@6.2          pcre@8.44               py-pyelftools@0.26    stc@0.8.3             umap@2.1.0
```

You can use `module load` or `spack load` to load the Spack packages in your user 
shell. Shown below are the modulefiles that correspond to the Spack packages.

```
--------------------------------------------------------------------- /global/common/software/spackecp/e4s-20.10/modules/cray-cnl7-haswell/ ----------------------------------------------------------------------
adiak/0.1.1-intel-19.1.2.254-mthjgtd2            hdf5/1.10.6-intel-19.1.2.254-rsmkvdoy            mpich/3.1-intel-19.1.2.254-wcezuimd              qthreads/1.14-intel-19.1.2.254-txp7dp43
adios2/2.6.0-intel-19.1.2.254-n4dtk4qs           hdf5/1.10.7-intel-19.1.2.254-eiobyugs            ncurses/6.2-intel-19.1.2.254-g46l7apt            raja/0.12.1-intel-19.1.2.254-clqqli2d
adlbx/0.9.2-intel-19.1.2.254-xlbmanu5            hdf5/1.10.7-intel-19.1.2.254-pak7dug3            netcdf-c/4.7.4-intel-19.1.2.254-rhqjykkt         rankstr/0.0.2-intel-19.1.2.254-vcnlhhbr
aml/0.1.0-intel-19.1.2.254-plk72jlf              hwloc/2.2.0-intel-19.1.2.254-2gyzs5sq            nettle/3.4.1-intel-19.1.2.254-2jcug5xh           readline/8.0-intel-19.1.2.254-ngqzpr6g
ant/1.10.7-intel-19.1.2.254-l4thkax6             hypre/2.18.2-intel-19.1.2.254-bsglrarm           ninja/1.10.1-intel-19.1.2.254-r72lgss7           redset/0.0.3-intel-19.1.2.254-bscvmphb
arborx/0.9-beta-intel-19.1.2.254-dyxgypgb        hypre/2.20.0-intel-19.1.2.254-pok5xpdu           nlohmann-json/3.9.1-intel-19.1.2.254-a4rrsbub    shuffile/0.0.3-intel-19.1.2.254-mibevhqg
argobots/1.0-intel-19.1.2.254-attdurbf           hypre/2.20.0-intel-19.1.2.254-rexbzxrs           nlopt/2.6.1-intel-19.1.2.254-wpao7epm            slepc/3.14.0-intel-19.1.2.254-vvdrsxyf
arpack-ng/3.7.0-intel-19.1.2.254-mgnqvw4p        intel-mkl/19.1.2.254-intel-19.1.2.254-3qgi4fgi   numactl/2.0.12-intel-19.1.2.254-fnfircl5         snappy/1.1.8-intel-19.1.2.254-24xez3cd
autoconf/2.69-intel-19.1.2.254-a4qx443y          kokkos/3.2.00-intel-19.1.2.254-4ofwm6dx          openjdk/11.0.2-intel-19.1.2.254-qil7jsta         sqlite/3.31.1-intel-19.1.2.254-iuxytzwg
automake/1.16.2-intel-19.1.2.254-n45gaypf        kokkos/3.2.00-intel-19.1.2.254-jamijalq          openpmd-api/0.12.0-intel-19.1.2.254-udgolvev     stc/0.8.3-intel-19.1.2.254-bz4n47ji
axl/0.3.0-intel-19.1.2.254-r2fck4xo              kokkos-kernels/3.2.00-intel-19.1.2.254-p2araoex  openssl/1.1.1g-intel-19.1.2.254-we2bo3po         suite-sparse/5.7.2-intel-19.1.2.254-3x5kozds
bolt/1.0-intel-19.1.2.254-ymgdfuke               kvtree/1.0.2-intel-19.1.2.254-ob5jw5ci           papi/6.0.0.1-intel-19.1.2.254-3hfse4rg           sundials/5.4.0-intel-19.1.2.254-242wjqcw
boost/1.74.0-intel-19.1.2.254-e7zdgsa2           libbsd/0.10.0-intel-19.1.2.254-w4tvpvc2          papi/6.0.0.1-intel-19.1.2.254-u4n73q6y           superlu/5.2.1-intel-19.1.2.254-lmqmz7ov
bzip2/1.0.8-intel-19.1.2.254-5uwgsvyl            libfabric/1.11.0-intel-19.1.2.254-rilhzysw       parallel-netcdf/1.12.1-intel-19.1.2.254-tzhf2347 superlu-dist/6.3.0-intel-19.1.2.254-ws2nqrqd
c-blosc/1.17.0-intel-19.1.2.254-426olrob         libffi/3.3-intel-19.1.2.254-plapxdrp             parmetis/4.0.3-intel-19.1.2.254-pjltphmg         superlu-dist/6.3.1-intel-19.1.2.254-flp5bkyt
caliper/2.4.0-intel-19.1.2.254-msrwlvgo          libiconv/1.16-intel-19.1.2.254-hidqyej2          pcre/8.44-intel-19.1.2.254-eozxjzaz              swig/4.0.2-intel-19.1.2.254-xdqcts7d
cmake/3.16.5-intel-19.1.2.254-zmyp2sa4           libnrm/0.1.0-intel-19.1.2.254-lqmtrfxi           pdt/3.25.1-intel-19.1.2.254-snfwngyt             sz/2.0.2.0-intel-19.1.2.254-xqpf6msr
darshan-runtime/3.2.1-intel-19.1.2.254-pxw7io3x  libpfm4/4.11.0-intel-19.1.2.254-53jyek3i         perl/5.30.3-intel-19.1.2.254-e7wxeve6            sz/2.1.10-intel-19.1.2.254-c2juglm6
darshan-util/3.2.1-intel-19.1.2.254-zcw5xq6e     libpng/1.6.37-intel-19.1.2.254-emjubioq          petsc/3.13.6-intel-19.1.2.254-expurd2b           tasmanian/7.3-intel-19.1.2.254-7rmkges7
diffutils/3.7-intel-19.1.2.254-6gqao3ig          libpthread-stubs/0.4-intel-19.1.2.254-azmwahqt   petsc/3.14.0-intel-19.1.2.254-3sfdqh37           tcl/8.6.10-intel-19.1.2.254-4oofcvju
er/0.0.3-intel-19.1.2.254-fcfjscln               libquo/1.3.1-intel-19.1.2.254-znhzfnct           pkg-config/0.29.2-intel-19.1.2.254-nxb62ywk      texinfo/6.5-intel-19.1.2.254-snawrbph
exmcutils/0.5.7-intel-19.1.2.254-bpoijynb        libsodium/1.0.18-intel-19.1.2.254-l4lmt4z2       pumi/2.2.2-intel-19.1.2.254-nrkupeqc             turbine/1.2.3-intel-19.1.2.254-pfwk2pgn
expat/2.2.9-intel-19.1.2.254-7zsw6ohk            libszip/2.1.1-intel-19.1.2.254-pqwulucm          pumi/2.2.2-intel-19.1.2.254-u7ra5bw2             turbine/1.2.3-intel-19.1.2.254-w4oqxhaj
flit/2.1.0-intel-19.1.2.254-6ufrb2kb             libtool/2.4.6-intel-19.1.2.254-w2kuzsym          py-cython/0.29.21-intel-19.1.2.254-yjdamuq7      umap/2.1.0-intel-19.1.2.254-edgoxhxd
gasnet/2020.3.0-intel-19.1.2.254-5nbbm7ww        libunwind/1.4.0-intel-19.1.2.254-dycdrifr        py-libensemble/0.7.0-intel-19.1.2.254-bn6b6rft   umpire/4.0.1-intel-19.1.2.254-fzo7wz7a
gdbm/1.18.1-intel-19.1.2.254-yzrxncmk            libuuid/1.0.3-intel-19.1.2.254-o532npji          py-mpi4py/3.0.3-intel-19.1.2.254-uprarn3p        umpire/4.0.1-intel-19.1.2.254-jxoi4b3o
gettext/0.21-intel-19.1.2.254-bqwcqalw           libzmq/4.3.2-intel-19.1.2.254-y4oux2w3           py-numpy/1.19.2-intel-19.1.2.254-yb6jdj2c        upcxx/2020.3.0-intel-19.1.2.254-fy3g3xtz
ginkgo/1.2.0-intel-19.1.2.254-i47x2ykg           lz4/1.9.2-intel-19.1.2.254-hiecokwj              py-petsc4py/3.13.0-intel-19.1.2.254-kkpcajdr     veloc/1.4-intel-19.1.2.254-rqqesf7e
globalarrays/5.7-intel-19.1.2.254-r3tzykgy       m4/1.4.18-intel-19.1.2.254-ws5y7ojc              py-pybind11/2.5.0-intel-19.1.2.254-pqflmoh4      xz/5.2.5-intel-19.1.2.254-5wcxpivv
gmake/4.2.1-intel-19.1.2.254-btxjbfbo            memkind/1.7.0-intel-19.1.2.254-gepq4n7u          py-pyelftools/0.26-intel-19.1.2.254-dpismpc4     zfp/0.5.5-intel-19.1.2.254-j5jmvakn
gmp/6.1.2-intel-19.1.2.254-tpii7i4j              mercury/1.0.1-intel-19.1.2.254-uetearts          py-scipy/1.5.3-intel-19.1.2.254-fi7tpgqt         zlib/1.2.11-intel-19.1.2.254-54rpg6zv
gnutls/3.6.14-intel-19.1.2.254-7hewruks          metis/5.1.0-intel-19.1.2.254-lzrewviy            py-setuptools/50.1.0-intel-19.1.2.254-psdd3j7c   zsh/5.8-intel-19.1.2.254-65ncemzz
gotcha/1.0.3-intel-19.1.2.254-nrye7psr           mfem/4.1.0-intel-19.1.2.254-ddu6klv6             py-toml/0.10.0-intel-19.1.2.254-sufxlrxs         zstd/1.4.5-intel-19.1.2.254-r27ln7zo
gslib/1.0.6-intel-19.1.2.254-gw3cc5gu            mpark-variant/1.4.0-intel-19.1.2.254-q2drubnz    python/3.8.6-intel-19.1.2.254-7b664mwe
```

We provide a buildcache to allow users to install specs in their user environment without
reinstalling the spec from source. In order to use buildcache one needs to configure a GPG key. 

By default, Spack stores the GPG key at `$SPACK_ROOT/opt/spack/gpg` which is not the ideal location 
for non-staff users, since they don't have write permissions to the central Spack instance. 
Instead we recommend you set `SPACK_GNUPGHOME` to an alternate path, preferably your
default GPG location (`$HOME/.gnupg`). This will enable Spack to trust your GPG key. 
To get started you can set **SPACK_GNUPGHOME** as follows:

```bash
# bash/sh/zsh users
export SPACK_GNUPGHOME=$HOME/.gnupg

# tcsh/csh users
setenv SPACK_GNUPGHOME $HOME/.gnupg
```

Next, we need to trust the public key `e4s2010.pub` by running the `spack gpg trust` command.

This will make Spack trust the public key, which is needed to install specs from
the buildcache, because they are signed by the public key.

```bash
$ spack gpg trust /global/common/software/spackecp/gpgkeys/e4s2010.pub   
gpgconf: socketdir is '/run/user/92503/gnupg'
gpg: key 0140A256659E0CBD: public key "Spack GPG Key (Spack E4S GPG Key) <shahzebsiddiqui@lbl.gov>" imported
gpg: Total number processed: 1
gpg:               imported: 1
```

You can confirm if the GPG key is imported by running the following:

```bash
$ spack gpg list
gpgconf: socketdir is '/run/user/92503/gnupg'
/global/cscratch1/sd/siddiq90/spack/opt/spack/gpg/pubring.kbx
-------------------------------------------------------------
pub   rsa2048 2020-09-22 [SCEA]
      EA172EB6343D30750A56522F0140A256659E0CBD
uid           [ unknown] Spack GPG Key (Spack E4S GPG Key) <shahzebsiddiqui@lbl.gov>
sub   rsa2048 2020-09-22 [SEA]
```

## Redeploying the stack

If you want an **exact replica** of the E4S stack, you should recreate a spack 
environment from the `spack.lock` file. 

For this example we will create a Spack environment in our home directory since 
as a user, you don't have permission to create an environment in the Spack instance. First 
we navigate to the `e4s-2010` environment and create an environment using `spack.lock`.

```bash
spack cd -e e4s-2010
spack env create -d $HOME/e4s-2010-user spack.lock
```

Next, we navigate to the user environment directory and we see a `spack.lock` and `spack.yaml`. We 
can activate the Spack environment using `spack env activate` command.

```bash
$ cd $HOME/e4s-2010-user
$ ls
spack.lock  spack.yaml
$ spack env activate .
$ spack env st
==> Using spack.yaml in current directory: /global/u1/s/siddiq90/e4s-2010-user
```

We should add the buildcache mirror so we can install from the buildcache. This can be done 
by running:

```bash
spack mirror add e4s2010 /global/common/software/spackecp/mirrors/e4s-2020-10/
```

Let's confirm our active mirrors by running:

```bash
$ spack mirror list
spack-public    https://spack-llnl-mirror.s3-us-west-2.amazonaws.com/
e4s2010         file:///global/common/software/spackecp/mirrors/e4s-2020-10
```

Now we can install from the mirror. This operation will take some time as it
will install all the packages from `spack.lock`.

```
spack install --cache-only
```

Once you have installed everything, you can run `spack find` and see the available specs
in your Spack environment. Now you are ready to install any additional specs in your
environment to satisfy your build dependency. 

```bash
$ spack find
==> In environment /global/u1/s/siddiq90/e4s-2010-user
==> Root specs
-- cray-cnl7-haswell / intel@19.1.2.254 -------------------------
adios2@2.6.0%intel@19.1.2.254 
aml@0.1.0%intel@19.1.2.254 
arborx@0.9-beta%intel@19.1.2.254 +openmp
bolt@1.0%intel@19.1.2.254 
caliper@2.4.0%intel@19.1.2.254 
darshan-runtime@3.2.1%intel@19.1.2.254 +slurm
darshan-util@3.2.1%intel@19.1.2.254 +bzip2
flit@2.1.0%intel@19.1.2.254 
gasnet@2020.3.0%intel@19.1.2.254 +udp
ginkgo@1.2.0%intel@19.1.2.254 
globalarrays@5.7%intel@19.1.2.254 +blas+lapack+scalapack
gotcha@1.0.3%intel@19.1.2.254 +test
hdf5@1.10.6%intel@19.1.2.254 +cxx+debug+fortran+hl+szip+threadsafe
hypre@2.20.0%intel@19.1.2.254 +mixedint
kokkos@3.2.00%intel@19.1.2.254 +compiler_warnings+debug+debug_dualview_modify_check+examples+hwloc+memkind+numactl+openmp+pic+tests
kokkos-kernels@3.2.00%intel@19.1.2.254 +mkl+openmp
libnrm@0.1.0%intel@19.1.2.254 
libquo@1.3.1%intel@19.1.2.254 
mercury@1.0.1%intel@19.1.2.254 +udreg
mfem@4.1.0%intel@19.1.2.254 +examples+gnutls+gslib+lapack+libunwind+openmp+pumi+threadsafe+umpire
ninja@1.10.1%intel@19.1.2.254 
openpmd-api@0.12.0%intel@19.1.2.254 
papi@6.0.0.1%intel@19.1.2.254 +example+static_tools
parallel-netcdf@1.12.1%intel@19.1.2.254 
pdt@3.25.1%intel@19.1.2.254 +pic
petsc@3.14.0%intel@19.1.2.254 
pumi@2.2.2%intel@19.1.2.254 +fortran
py-libensemble@0.7.0%intel@19.1.2.254 +mpi+nlopt+scipy
py-petsc4py@3.13.0%intel@19.1.2.254 
qthreads@1.14%intel@19.1.2.254 ~hwloc
raja@0.12.1%intel@19.1.2.254 
slepc@3.14.0%intel@19.1.2.254 
stc@0.8.3%intel@19.1.2.254 
sundials@5.4.0%intel@19.1.2.254 +examples-cxx+examples-f2003~examples-f77+f2003+hypre+klu+lapack+openmp
superlu@5.2.1%intel@19.1.2.254 
superlu-dist@6.3.1%intel@19.1.2.254 
swig@4.0.2%intel@19.1.2.254 
sz@2.1.10%intel@19.1.2.254 +fortran+hdf5+netcdf+pastri+python+random_access+time_compression
tasmanian@7.3%intel@19.1.2.254 +blas+fortran+mpi+python+xsdkflags
turbine@1.2.3%intel@19.1.2.254 +hdf5+python
umap@2.1.0%intel@19.1.2.254 +tests
umpire@4.0.1%intel@19.1.2.254 +fortran+numa+openmp
upcxx@2020.3.0%intel@19.1.2.254 
veloc@1.4%intel@19.1.2.254 
zfp@0.5.5%intel@19.1.2.254 

==> 135 installed packages
-- cray-cnl7-haswell / intel@19.1.2.254 -------------------------
adiak@0.1.1      darshan-runtime@3.2.1  hdf5@1.10.6            libpfm4@4.11.0        mpich@3.1               perl@5.30.3           qthreads@1.14       sz@2.1.10
adios2@2.6.0     darshan-util@3.2.1     hdf5@1.10.7            libpng@1.6.37         ncurses@6.2             petsc@3.13.6          raja@0.12.1         tasmanian@7.3
adlbx@0.9.2      diffutils@3.7          hdf5@1.10.7            libpthread-stubs@0.4  netcdf-c@4.7.4          petsc@3.14.0          rankstr@0.0.2       tcl@8.6.10
aml@0.1.0        er@0.0.3               hwloc@2.2.0            libquo@1.3.1          nettle@3.4.1            pkg-config@0.29.2     readline@8.0        texinfo@6.5
ant@1.10.7       exmcutils@0.5.7        hypre@2.18.2           libsodium@1.0.18      ninja@1.10.1            pumi@2.2.2            redset@0.0.3        turbine@1.2.3
arborx@0.9-beta  expat@2.2.9            hypre@2.20.0           libszip@2.1.1         nlohmann-json@3.9.1     pumi@2.2.2            shuffile@0.0.3      turbine@1.2.3
argobots@1.0     flit@2.1.0             hypre@2.20.0           libtool@2.4.6         nlopt@2.6.1             py-cython@0.29.21     slepc@3.14.0        umap@2.1.0
arpack-ng@3.7.0  gasnet@2020.3.0        intel-mkl@19.1.2.254   libunwind@1.4.0       numactl@2.0.12          py-libensemble@0.7.0  snappy@1.1.8        umpire@4.0.1
autoconf@2.69    gdbm@1.18.1            kokkos@3.2.00          libuuid@1.0.3         openjdk@11.0.2          py-mpi4py@3.0.3       sqlite@3.31.1       umpire@4.0.1
automake@1.16.2  gettext@0.21           kokkos@3.2.00          libzmq@4.3.2          openpmd-api@0.12.0      py-numpy@1.19.2       stc@0.8.3           upcxx@2020.3.0
axl@0.3.0        ginkgo@1.2.0           kokkos-kernels@3.2.00  lz4@1.9.2             openssl@1.1.1g          py-petsc4py@3.13.0    suite-sparse@5.7.2  veloc@1.4
bolt@1.0         globalarrays@5.7       kvtree@1.0.2           m4@1.4.18             papi@6.0.0.1            py-pybind11@2.5.0     sundials@5.4.0      xz@5.2.5
boost@1.74.0     gmake@4.2.1            libbsd@0.10.0          memkind@1.7.0         papi@6.0.0.1            py-pyelftools@0.26    superlu@5.2.1       zfp@0.5.5
bzip2@1.0.8      gmp@6.1.2              libfabric@1.11.0       mercury@1.0.1         parallel-netcdf@1.12.1  py-scipy@1.5.3        superlu-dist@6.3.0  zlib@1.2.11
c-blosc@1.17.0   gnutls@3.6.14          libffi@3.3             metis@5.1.0           parmetis@4.0.3          py-setuptools@50.1.0  superlu-dist@6.3.1  zsh@5.8
caliper@2.4.0    gotcha@1.0.3           libiconv@1.16          mfem@4.1.0            pcre@8.44               py-toml@0.10.0        swig@4.0.2          zstd@1.4.5
cmake@3.16.5     gslib@1.0.6            libnrm@0.1.0           mpark-variant@1.4.0   pdt@3.25.1              python@3.8.6          sz@2.0.2.0
```
